# Switch Ubuntu Desktop to mainline kernel 

Dieses Projekt stellt Skripte zur Verfügung um die Desktop Variante von Ubuntu zu zwingen auf dem Mainline Kernel zu bleiben, statt alle 6 Monate eine neue Kernelversion über den Harware-Enablement-Kernel (hwe) zu verwenden.

## GRUB boot menu aktivieren

Im Zweifel kann man vor dem Login mit `STRG+ALT+F3` in die Console wechseln und sich dort einloggen.

Um beim Boot ein GRUB Menu zu bekommen müssen folgede Werte in `/etc/default/grub` angepasst werden:
```
#GRUB_TIMEOUT_STYLE=hidden
#GRUB_TIMEOUT=0
GRUB_TIMEOUT_STYLE=menu
GRUB_TIMEOUT=3
```

Danach folgende Befehle ausführen um die Änderungen zu aktivieren und zu rebooten:
```
sudo update-grub
sudo reboot
```

Beim Reboot dann in einen Kernel Booten der keine Probleme bereitet (Zum Beispiel 5.4 bei dem i915 GPU hang Problem).


## Fix script herunterladen und ausführen

### Via CLI

```
wget https://gitli.stratum0.org/heyalter/switch-ubuntu-desktop-to-mainline-kernel/-/raw/master/switch_to_mainline_kernel.sh
chmod +x switch_to_mainline_kernel.sh
sudo ./switch_to_mainline_kernel.sh
```

### Via Browser

* Script herunterladen https://gitli.stratum0.org/heyalter/switch-ubuntu-desktop-to-mainline-kernel/-/raw/master/switch_to_mainline_kernel.sh?inline=false
