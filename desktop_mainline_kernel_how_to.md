# Hey, Alter! i915 Grafik-Treiber Bug Workaround

Software Fehler in im Betirebsystem eines Rechner kommen vor - seltener sind schwere Fehler die einen Rechner unbenutzbar machen können. Leider sind einige der HEY, ALTER! Rechner genau von einem solchen Bug betroffen.

Symptome: Der Rechner startet normal und zeigt die Login Eingabefelder. Nach dem Login erscheint der Desktop. Bei dem Versuch ein Programm zu starten hängt sich die grafische Oberfläche auf und reagiert nicht mehr. Eventuell startet die grafische Oberfläche neu und zeigt erneut den Login.

*Wenn sie bereits von dem oben beschriebenen Porblem betroffen sind folgend sie Bitte zunächst der Anleitung hinter diesem Link: [alternativen_kernel_booten.md](./alternativen_kernel_booten.md). Kehren sie danach hierher zurück um den permanenten Workaround aus dieser Anleitung nutzen zu können.*

Nutzen sie zum ausführen der Anleitung den HEY, ALTER! Rechner:

1. Klicken sie den folgenden Link um die die benötigten Dateien herunter zu laden: [switch_to_mainline_kernel.zip](https://gitli.stratum0.org/heyalter/desktop-mainline-kernel/-/jobs/artifacts/master/download?job=pack)
2. Wenn die Datei heruntergeladen wurde wählen sie die Datei unten im Browser aus und klicken sie auf `Öffnen`:
	![alt text](img/switch_kernel_1.png "Download")
3. Wählen sie in dem erscheinendem Fenster "Öffnen" aus:
	![alt text](img/switch_kernel_2.png "Archivverwaltung")
4. Wählen sie im nächsten Fenster `Entpacken` in der linken oberen Ecke:
	![alt text](img/switch_kernel_3.png "Entpacken")
5. Wählen sie im nächsten Fenster erneut `Entpacken`:
	![alt text](img/switch_kernel_4.png "Entpacken")
6. Klicken sie auf `Dateien anziegen` sobald das Archiv entpackt wurde:
	![alt text](img/switch_kernel_5.png "Dateien anzeigen")
7. In dem sich öffnenden Fenster klicken sie auf den Knopf mit den 3 waagerechten Strichen und wählen sie den Menüpunkt `Einstellungen`:
	![alt text](img/switch_kernel_6.png "Hamburger")
8. Wählen sie den Reiter `Verhalten` aus und klicken sie auf `Ausführbare Dateien` -> `Nachfragen, was geschehen soll`. Danach schließen sie das Fenster über des `X` rechts oben:
	![alt text](img/switch_kernel_7.png "Nachfragen")
9. Öffnen sie die Datei `switch_to_mainline_kernel.sh` durch Doppelklick:
	![alt text](img/switch_kernel_8.png "Open")
10. Wählen sie in dem Dialog `Im Terminal öffnen`:
	![alt text](img/switch_kernel_9.png "Open Terminal")
11. In dem sich öffnenden Fenster werden sie aufgeforedert ein das Passwort für den Nutzer `schule` einzugeben. Im Auslieferungszustand ist das Passwort ebenfalls `schule`. Geben sie das Passwort ein und bestätigen sie die Eingabe mit der `Enter` Taste. (Das Passwort wird bei der Eingabe nicht angezeigt.)
	![alt text](img/switch_kernel_10.png "Script [ausführen]")
12. Wenn sie in dem Fenster Meldungen bezüglich einer `Sperrdatei` sehen, dann lädt ihr Rechner gerade automatische Updates. Dies kann einige Zeit in Anspruch nehmen. Schließen sie das Fenster und wiederholen sie die Anleitung ab Schriit 9. später erneut.
13. Wenn das Skript bis zum Ende durch läuft sehen sie die Folgende Meldung:
	![alt text](img/switch_kernel_11.png "Press Enter")
14. Sie können den Rechner nun neustarten und normal benutzen.
