# Alternativen Kernel booten

Wenn der HEY, ALTER! Rechner bereits keine Programme mehr startet, folgen sie bitte dieser Anleitung! Falls sie auf Probleme stoßen, oder sich nicht in der Lage sehen die Anleitung auszuführen, kontaktieren sie bitte ihre lokale HEY, ALTER! Die Email-Adressen der lokalen Initiativen finden sie auf [https://heyalter.com](https://heyalter.com)

Starten sie den Rechner neu und warten sie bis der Login Screen erscheint:

![alt text](img/1_login_screen.png "Login screen")

Statt sich einzuloggen drücken sie bitte die Tastenkombination `STRG+ALT+F3`. Der folgende Bildschirm erscheint:

![alt text](img/2_cli_login.png "cli login")

Hier geben sie bitte für den `ubuntu login: ` wie gewohnt den Nutzernamen `schule` ein und bestätigen mit der Enter-Taste. Sobald die Abfrage für das Passwort erscheint geben sie hier ebenfalls `schule` ein (die Buchstaben erscheinen nicht auf dem Bildschirm) und bestätigen mit der Enter-Taste. (Sollten sie das Passwort in der Vergangenheit geändert haben geben sie bitte ihr Passwort ein).

![alt text](img/3_console.png "console")

Geben sie nun den Befehlt `sudo nano /etc/default/grub` ein.

![alt text](img/4_nano.png "nano")

Es erscheint ein Textbearbeitungsprogram in dem sie mit den Pfeiltasten auf der Tastatur navigieren können.

![alt text](img/5_grub_orig.png "grub original")

Ändern sie bitte die folgenden Zeilen entpsrechend ab:
- von `GRUB_TIMEOUT_STYLE=hidden` nach `GRUB_TIMEOUT_STYLE=menu`
- von `GRUB_TIMEOUT=0` nach `GRUB_TIMEOUT=3`

Drücken sie dann die Tastenkombination: `STRG+X`. Bestätigen sie das Speichern mit `J` und drücken sie danach die Entertaste.

![alt text](img/6_grub_mod.png "grub modified")

Nun geben sie den Befehl `sudo update-grub` ein und führen sie ihn mit der Entertaste aus.

![alt text](img/7_update_grub.png "update grub")

Es erscheinen die folgenden Meldungen:

![alt text](img/8_update_grub_output.png "update grub output")

Starten sie den Rechner mit dem Kommando `sudo reboot` neu.

![alt text](img/9_reboot.png "reboot")

Beim Start des Rechners erscheint nun ein Auswahlmenu. Sie haben 3 Sekunden Zeit um mit der Pfeiltaste den Punkt `Erweiterte Optionen für Ubuntu` auszuwählen. Bestätigen sie mit `Enter`

![alt text](img/10_grub_menu.png "grub menu")

Es erscheint ein weiteres Menu in dem sie die erste Zeile auswählen die `Linux 5.4` enthält.

![alt text](img/11_grub_5_4_kernel.png "grub_5_4_kernel")

Der Rechner startet nun normal und sie können wieder alle Programme nutzen. Damit sie sie nicht bei jedem Start die Version 5.4 auswählen müssen folgen sie bitte noch der Anleitung ![desktop_mainline_kernel_how_to.md](desktop_mainline_kernel_how_to.md).
